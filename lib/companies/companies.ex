defmodule Companies.Company do
  use Ecto.Schema
  import Ecto.Changeset

  schema "companies" do
    field(:name, :string)
    field(:ceo, :string)
    has_many(:shops, Companies.Shop)
    timestamps()
  end

  def changeset(company, params \\ :empty) do
    company
    |> cast(params, [
      :name, :ceo
    ])
    |> validate_required([:name, :ceo])
  end

  def create(params) do
    %Companies.Company{}
    |> changeset(params)
    |> Companies.Repo.insert!()
  end

  def update(company, params) do
    company
    |> changeset(params)
    |> Companies.Repo.update!()
  end

  def delete(company) do
    company
    |> Companies.Repo.delete!()
  end
end
