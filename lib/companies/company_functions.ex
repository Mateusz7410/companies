defmodule Companies.CompanyFunctions do
  import Ecto.Query

  def get_long() do
    Companies.Repo.all from c in Companies.Company, preload: [:shops]
  end

  def get_long(id) do
    res =
    (Companies.Repo.all from c in Companies.Company, preload: [:shops], where: c.id == ^id)
    |> Enum.fetch(0)
    case res do
      {:ok, company} ->
        company
      :error ->
        :error
    end
  end

  def get_short() do
    Companies.Repo.all from c in Companies.Company
  end

  def get_short(id) do
    res =
    (Companies.Repo.all from c in Companies.Company, where: c.id == ^id)
    |> Enum.fetch(0)
    case res do
      {:ok, company} ->
        company
      :error ->
        :error
    end
  end

  def get_products(company_id) do
    case Companies.Repo.get_by(Companies.Company, id: company_id) do
      nil ->
        :error
      company ->
        products =
        (Companies.Repo.all from s in Companies.Shop, where: s.company_id == ^company_id, preload: [products_have_shops: [:product]])
        |> Enum.map(& &1.products_have_shops)
        |> List.flatten()

        %{
          company: company,
          products: products
        }
    end
  end

  def post(%{"name" => name, "ceo" => ceo, "token" => token}) do
    case Companies.TokenAgent.get(token) do
      nil ->
        :token
      token ->
        Companies.TokenAgent.delete(token)
        Companies.Company.create(%{name: name, ceo: ceo})
    end
  end

  def put(%{"id" => id, "name" => name, "ceo" => ceo, "token" => token}) do
    ans =
    case Companies.TokenAgent.get(id) do
      nil ->
        :ok
      token2 ->
        if token2 == token do
          :ok
        else
          token2
        end
    end

    case Companies.Repo.get_by(Companies.Company, id: id) do
      nil ->
        :error
      company ->
        if ans == :ok do
          Companies.Company.update(company, %{name: name, ceo: ceo})
          token = Companies.TokenController.random_string(64)
          Companies.TokenAgent.delete(id)
          Companies.TokenAgent.put(id, token)
          token
        else
          {:preconditon_failed, ans}
        end
    end
  end

  def put(%{"id" => id, "name" => name, "ceo" => ceo}) do
    case Companies.TokenAgent.get(id) do
      nil ->
        token = Companies.TokenController.random_string(64)
        Companies.TokenAgent.put(id, token)
        case Companies.Repo.get_by(Companies.Company, id: id) do
          nil ->
            :error
          company ->
            Companies.Company.update(company, %{name: name, ceo: ceo})
            token
        end
      token ->
        IO.inspect(token)
        IO.inspect(Companies.TokenAgent.get(id))
        {:precondition_required, token}
    end
  end

  def delete(id) do
    case Companies.Repo.get_by(Companies.Company, id: id) do
      nil ->
        :error
      company ->
        Companies.Company.delete(company)
    end
  end
end
