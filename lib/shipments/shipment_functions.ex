defmodule Companies.ShipmentFunctions do
  import Ecto.Query

  def get(shop_id, shipment_id) do
    case Companies.Repo.get_by(Companies.Shop, id: shop_id) do
      nil ->
        :error
      _shop ->
        res =
          (Companies.Repo.all from s in Companies.Shipment, where: s.id == ^shipment_id, preload: [products_have_shipments: [:product]])
          |> Enum.fetch(0)
        case res do
          {:ok, shipment} ->
            shipment
          :error ->
            :error
        end
    end
  end

  def get_all_from_company(company_id) do
    case Companies.Repo.get_by(Companies.Company, id: company_id) do
      nil ->
        :error
      _company ->
        products =
        (Companies.Repo.all from s in Companies.Shop, where: s.company_id == ^company_id, preload: [products_have_shops: [:product]])
        |> Enum.map(& &1.products_have_shops)
        |> List.flatten()
        |> Enum.map(& &1.product.id)

        (Companies.Repo.all from phs in Companies.ProductHasShipment, where: phs.product_id in ^products, preload: [:shipment])
        |> Enum.map(& &1.shipment)
        |> Enum.uniq()
    end
  end

  def get_all_from_shop(shop_id) do
    case Companies.Repo.get_by(Companies.Shop, id: shop_id) do
      nil ->
        :error
      _shop ->
        products =
        (Companies.Repo.all from s in Companies.Shop, where: s.id == ^shop_id, preload: [products_have_shops: [:product]])
        |> Enum.map(& &1.products_have_shops)
        |> List.flatten()
        |> Enum.map(& &1.product.id)

        (Companies.Repo.all from phs in Companies.ProductHasShipment, where: phs.product_id in ^products, preload: [:shipment, :product])
        |> Enum.map(& &1.shipment)
        |> Enum.uniq()
    end
  end

  def post(%{"shop_id" => shop_id, "products" => products}) do
    case Companies.Repo.get_by(Companies.Shop, id: shop_id) do
      nil ->
        :error
      _shop ->
        create_objects(products, shop_id)
    end
  end

  defp create_objects(products, shop_id) do
    shipment = Companies.Shipment.create(%{date: DateTime.utc_now()})
    create_objects(products, [], true, shipment, shop_id)
  end

  defp create_objects([], operations, valid, shipment, _shop_id) do
    if valid do
      :ok
    else
      operations
      |> Enum.map(fn operation ->
        if length(operation) == 3 do
          [prod, phship, phsop] = operation
          Companies.ProductHasShipment.delete(phship)
          Companies.ProductHasShop.delete(phsop)
          Companies.Product.delete(prod)
        else
          [phship, phsop] = operation
          Companies.ProductHasShipment.delete(phship)
          Companies.ProductHasShop.delete(phsop)
        end
      end)
      Companies.Shipment.delete(shipment)
      :error
    end
  end

  defp create_objects([product | products], operations, valid, shipment, shop_id) do
    operation =
    if product["id"] == -1 do
      new_product = Companies.Product.create(%{name: product["name"], price: product["price"]})
      phship = Companies.ProductHasShipment.create(%{product_id: new_product.id, shipment_id: shipment.id, quantity: product["quantity"]})
      phshop = Companies.ProductHasShop.create(%{product_id: new_product.id, shop_id: shop_id, quantity: product["quantity"]})
      [new_product, phship, phshop]
    else
      case Companies.Repo.get_by(Companies.Product, id: product["id"]) do
        nil ->
          false
        _product ->
          phship = Companies.ProductHasShipment.create(%{product_id: product["id"], shipment_id: shipment.id, quantity: product["quantity"]})
          phshop = Companies.ProductHasShop.create(%{product_id: product["id"], shop_id: shop_id, quantity: product["quantity"]})
          [phship, phshop]
      end
    end
    if operation == false do
      create_objects([], operations, operation, shipment, shop_id)
    else
      create_objects(products, [operation | operations], operation, shipment, shop_id)
    end
  end

  def delete(shipment_id) do
    res =
      (Companies.Repo.all from s in Companies.Shipment, where: s.id == ^shipment_id, preload: [products_have_shipments: [:product]])
      |> Enum.fetch(0)
    case res do
      {:ok, shipment} ->
        data =
        shipment.products_have_shipments
        |> Enum.map(& %{id: &1.id, quantity: &1.quantity})

        case delete_shipment_has_no_different_quantity(data) do
          false ->
            :error
          true ->
            delete_product_has_shop(data)
            Companies.Shipment.delete(shipment)
        end
      :error ->
        :error
    end
  end

  defp delete_shipment_has_no_different_quantity([]) do
    true
  end

  defp delete_shipment_has_no_different_quantity([d | data]) do
    case Companies.Repo.get_by(Companies.ProductHasShop, id: d.id) do
      nil ->
        false
      phs ->
        if phs.quantity != d.quantity do
          false
        else
          delete_shipment_has_no_different_quantity(data)
        end
    end
  end

  defp delete_product_has_shop([]) do
    :ok
  end

  defp delete_product_has_shop([d | data]) do
    Companies.Repo.get_by(Companies.ProductHasShop, id: d.id)
    |> Companies.ProductHasShop.delete()
    delete_product_has_shop(data)
  end
end
