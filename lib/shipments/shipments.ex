defmodule Companies.Shipment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "shipments" do
    field(:date, :utc_datetime)
    has_many(:products_have_shipments, Companies.ProductHasShipment)
    timestamps()
  end

  def changeset(shipment, params \\ :empty) do
    shipment
    |> cast(params, [
      :date
    ])
    |> validate_required([:date])
  end

  def create(params) do
    %Companies.Shipment{}
    |> changeset(params)
    |> Companies.Repo.insert!()
  end

  def update(shipment, params) do
    shipment
    |> changeset(params)
    |> Companies.Repo.update!()
  end

  def delete(shipment) do
    shipment
    |> Companies.Repo.delete!()
  end
end
