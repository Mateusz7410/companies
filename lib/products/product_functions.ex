defmodule Companies.ProductFunctions do
  import Ecto.Query

  def get(company_id, shop_id, pagination, token) do
    query = from(p in Companies.Product, join: phs in Companies.ProductHasShop, where: p.id == phs.product_id and phs.shop_id == ^shop_id, order_by: [asc: p.id])
    case pagination do
      "first" ->
        Companies.Repo.paginate(query, include_total_count: true, cursor_fields: [:inserted_at, :id], limit: 2)
      "after" ->
        Companies.Repo.paginate(query, after: token, include_total_count: true, cursor_fields: [:inserted_at, :id], limit: 2)
      "before" ->
        Companies.Repo.paginate(query, before: token, include_total_count: true, cursor_fields: [:inserted_at, :id], limit: 2)
      "last" ->
        last_page(query, nil)
      _ ->
        :error
    end
  end

  defp last_page(query, token) do
    %{entries: entries, metadata: metadata} = Companies.Repo.paginate(query, after: token, include_total_count: true, cursor_fields: [:inserted_at, :id], limit: 2)
    if metadata.after == nil do
      %{entries: entries, metadata: metadata}
    else
      last_page(query, metadata.after)
    end
  end

  def get(company_id, shop_id, product_id) do
    {product_id, _res} = Integer.parse(product_id)
    res =
      (Companies.Repo.all from s in Companies.Shop, where: s.company_id == ^company_id and s.id == ^shop_id, preload: [products_have_shops: [:product]])
      |> Enum.fetch(0)
    case res do
      {:ok, shop} ->
        res =
        shop.products_have_shops
        |> Enum.map(& &1.product)
        |> Enum.find_value(& if &1.id == product_id, do: &1)

        case res do
          nil ->
            :error
          product ->
            product
        end
      :error ->
        :error
    end
  end

  def post(%{"name" => name, "price" => price, "shop_id" => shop_id, "quantity" => quantity}) do
    product = Companies.Product.create(%{name: name, price: price})
    Companies.ProductHasShop.create(%{product_id: product.id, shop_id: shop_id, quantity: quantity})
  end

  def put(%{"name" => name, "price" => price, "product_id" => product_id}) do
    Companies.Repo.query!("set transaction isolation level serializable;")
    case Companies.Repo.get_by(Companies.Product, id: product_id) do
      nil ->
        :error
      product ->
        Companies.Product.update(product, %{name: name, price: price})
    end
  end

  def put(%{"name" => name, "product_id" => product_id}) do
    Companies.Repo.query!("set transaction isolation level serializable;")
    case Companies.Repo.get_by(Companies.Product, id: product_id) do
      nil ->
        :error
      product ->
        Companies.Product.update(product, %{name: name})
    end
  end

  def put(%{"price" => price, "product_id" => product_id}) do
    Companies.Repo.query!("set transaction isolation level serializable;")
    case Companies.Repo.get_by(Companies.Product, id: product_id) do
      nil ->
        :error
      product ->
        Companies.Product.update(product, %{price: price})
    end
  end

  def put(%{"shop_id" => shop_id, "product_id" => product_id, "quantity" => quantity}) do
    Companies.Repo.query!("set transaction isolation level serializable;")
    {product_id, _res} = Integer.parse(product_id)
    res =
      (Companies.Repo.all from s in Companies.Shop, where: s.id == ^shop_id, preload: [products_have_shops: [:product]])
      |> Enum.fetch(0)
    case res do
      {:ok, shop} ->
        res =
        shop.products_have_shops
        |> Enum.find_value(& if &1.product.id == product_id, do: &1)

        case res do
          nil ->
            :error
          product_has_shop ->
            Companies.ProductHasShop.update(product_has_shop, %{quantity: quantity})
        end
      :error ->
        :error
    end
  end

  def delete(product_id) do
    case Companies.Repo.get_by(Companies.Product, id: product_id) do
      nil ->
        :error
      product ->
        Companies.Product.delete(product)
    end
  end

  def delete(company_id, shop_id) do
    from(p in Companies.Product, join: phs in Companies.ProductHasShop, where: p.id == phs.product_id and phs.shop_id == ^shop_id)
    |> Companies.Repo.all()
    |> Enum.map(&Companies.Product.delete(&1))
  end
end
