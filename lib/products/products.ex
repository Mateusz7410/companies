defmodule Companies.Product do
  use Ecto.Schema
  import Ecto.Changeset

  schema "products" do
    field(:name, :string)
    field(:price, :decimal)
    has_many(:products_have_shops, Companies.ProductHasShop)
    has_many(:products_have_shipments, Companies.ProductHasShipment)
    timestamps()
  end

  def changeset(product, params \\ :empty) do
    product
    |> cast(params, [
      :name, :price
    ])
    |> validate_required([:name, :price])
  end

  def create(params) do
    %Companies.Product{}
    |> changeset(params)
    |> Companies.Repo.insert!()
  end

  def update(product, params) do
    product
    |> changeset(params)
    |> Companies.Repo.update!()
  end

  def delete(product) do
    product
    |> Companies.Repo.delete!()
  end
end
