defmodule Companies.ProductHasShop do
  use Ecto.Schema
  import Ecto.Changeset

  schema "products_have_shops" do
    belongs_to(:product, Companies.Product)
    belongs_to(:shop, Companies.Shop)
    field(:quantity, :integer)
    timestamps()
  end

  def changeset(product_has_shop, params \\ :empty) do
    product_has_shop
    |> cast(params, [
      :product_id, :shop_id, :quantity
    ])
    |> validate_required([:product_id, :shop_id, :quantity])
  end

  def create(params) do
    %Companies.ProductHasShop{}
    |> changeset(params)
    |> Companies.Repo.insert!()
  end

  def update(product_has_shop, params) do
    product_has_shop
    |> changeset(params)
    |> Companies.Repo.update!()
  end

  def delete(product_has_shop) do
    product_has_shop
    |> Companies.Repo.delete!()
  end
end
