defmodule Companies.ProductHasShipment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "products_have_shipments" do
    belongs_to(:product, Companies.Product)
    belongs_to(:shipment, Companies.Shipment)
    field(:quantity, :integer)
    timestamps()
  end

  def changeset(product_has_shipment, params \\ :empty) do
    product_has_shipment
    |> cast(params, [
      :product_id, :shipment_id, :quantity
    ])
    |> validate_required([:product_id, :shipment_id, :quantity])
  end

  def create(params) do
    %Companies.ProductHasShipment{}
    |> changeset(params)
    |> Companies.Repo.insert!()
  end

  def update(product_has_shipment, params) do
    product_has_shipment
    |> changeset(params)
    |> Companies.Repo.update!()
  end

  def delete(product_has_shipment) do
    product_has_shipment
    |> Companies.Repo.delete!()
  end
end
