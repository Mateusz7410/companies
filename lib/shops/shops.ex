defmodule Companies.Shop do
  use Ecto.Schema
  import Ecto.Changeset

  schema "shops" do
    belongs_to(:company, Companies.Company)
    field(:address, :string)
    field(:director, :string)
    has_many(:products_have_shops, Companies.ProductHasShop)
    timestamps()
  end

  def changeset(shop, params \\ :empty) do
    shop
    |> cast(params, [
      :company_id, :address, :director
    ])
    |> validate_required([:company_id, :address, :director])
  end

  def create(params) do
    %Companies.Shop{}
    |> changeset(params)
    |> Companies.Repo.insert!()
  end

  def update(shop, params) do
    shop
    |> changeset(params)
    |> Companies.Repo.update!()
  end

  def delete(shop) do
    shop
    |> Companies.Repo.delete!()
  end
end
