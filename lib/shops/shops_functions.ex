defmodule Companies.ShopFunctions do
  import Ecto.Query

  def get_long(company_id) do
    Companies.Repo.all from s in Companies.Shop, preload: [:company, products_have_shops: [:product]], where: s.company_id == ^company_id
  end

  def get_long(company_id, shop_id) do
    res =
    (Companies.Repo.all from s in Companies.Shop, preload: [:company, products_have_shops: [:product]], where: s.id == ^shop_id and s.company_id == ^company_id)
    |> Enum.fetch(0)
    case res do
      {:ok, shop} ->
        shop
      :error ->
        :error
    end
  end

  def get_short(company_id) do
    Companies.Repo.all from s in Companies.Shop, where: s.company_id == ^company_id
  end

  def get_short(company_id, shop_id) do
    res =
    (Companies.Repo.all from s in Companies.Shop, where: s.id == ^shop_id and s.company_id == ^company_id)
    |> Enum.fetch(0)
    case res do
      {:ok, shop} ->
        shop
      :error ->
        :error
    end
  end

  def post(%{"address" => address, "director" => director, "company_id" => company_id}) do
    case Companies.Repo.get_by(Companies.Company, id: company_id) do
      nil ->
        :error
      _company ->
        Companies.Shop.create(%{address: address, director: director, company_id: company_id})
    end
  end

  def put(%{"shop_id" => shop_id, "address" => address, "director" => director, "company_id" => company_id}) do
    Companies.Repo.query!("set transaction isolation level serializable;")
    case Companies.Repo.get_by(Companies.Shop, id: shop_id) do
      nil ->
        :error
      shop ->
        case Companies.Repo.get_by(Companies.Company, id: company_id) do
          nil ->
            :error
          _company ->
            Companies.Shop.update(shop, %{address: address, director: director})
        end
    end
  end

  def delete(company_id, shop_id) do
    case Companies.Repo.get_by(Companies.Shop, id: shop_id) do
      nil ->
        :error
      shop ->
        case Companies.Repo.get_by(Companies.Company, id: company_id) do
          nil ->
            :error
          _company ->
            Companies.Shop.delete(shop)
        end
    end
  end
end
