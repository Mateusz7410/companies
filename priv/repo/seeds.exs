# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Companies.Repo.insert!(%Companies.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

samsung = Companies.Company.create(%{name: "Samsung", ceo: "Kim Hyun Suk"})
lenovo = Companies.Company.create(%{name: "Lenovo", ceo: "Yang Yuanqing"})
biedronka = Companies.Company.create(%{name: "Biedronka", ceo: "Jeronimo Martins"})

shop1samsung = Companies.Shop.create(%{company_id: samsung.id, address: "ul. Poznańska 62, 60-645 Poznań", director: "Andrzej Sucholeski"})
shop2samsung = Companies.Shop.create(%{company_id: samsung.id, address: "ul. Wiejska 13, 14-432 Warszawa", director: "Witold Poziomek"})
shop3samsung = Companies.Shop.create(%{company_id: samsung.id, address: "ul. Wierzbowa 12, 60-645 Kraków", director: "Seweryn Krzaczasty"})

shop1lenovo = Companies.Shop.create(%{company_id: lenovo.id, address: "ul. Mniejsza 1, 43-123 Rzeszów", director: "Marcin Krot"})
shop2lenovo = Companies.Shop.create(%{company_id: lenovo.id, address: "ul. Proporcjonalna 143, 60-325 Suchy Las", director: "Jolanta Mater"})
shop3lenovo = Companies.Shop.create(%{company_id: lenovo.id, address: "ul. Werbel 32, 12-322 Rakownia", director: "Mateusz Swojski"})

shop1biedronka = Companies.Shop.create(%{company_id: biedronka.id, address: "ul. Bitwy Wielkiej 74, 15-231 Ciechocinek", director: "Karol Bezimienny"})
shop2biedronka = Companies.Shop.create(%{company_id: biedronka.id, address: "ul. Normalna 532, 43-123 Brzegi Górne", director: "Weronika Biedrońska"})
shop3biedronka = Companies.Shop.create(%{company_id: biedronka.id, address: "ul. Ciekawa 73, 87-463 Katowice", director: "Angelika Wiechowicz"})

product1shop1samsung = Companies.Product.create(%{name: "Galaxy Note 7", price: 1000})
product1shop2samsung = Companies.Product.create(%{name: "Galaxy 7", price: 1200})
product1shop3samsung = Companies.Product.create(%{name: "Galaxy A7", price: 1300})
product2shop1samsung = Companies.Product.create(%{name: "Galaxy Note 8", price: 1800})
product2shop2samsung = Companies.Product.create(%{name: "Galaxy 8", price: 1600})
product2shop3samsung = Companies.Product.create(%{name: "Galaxy A8", price: 1400})
product3shop1samsung = Companies.Product.create(%{name: "Galaxy Note 9", price: 2500})
product3shop2samsung = Companies.Product.create(%{name: "Galaxy 9", price: 2100})
product3shop3samsung = Companies.Product.create(%{name: "Galaxy A9", price: 1900})

product1shop1lenovo = Companies.Product.create(%{name: "Thinkpad Carbon 6th", price: 5000})
product1shop2lenovo = Companies.Product.create(%{name: "Thinkpad W580", price: 2300})
product1shop3lenovo = Companies.Product.create(%{name: "Thinkpad X210", price: 2400})
product2shop1lenovo = Companies.Product.create(%{name: "Thinkpad Carbon 5th", price: 4500})
product2shop2lenovo = Companies.Product.create(%{name: "Thinkpad W570", price: 2000})
product2shop3lenovo = Companies.Product.create(%{name: "Thinkpad X215", price: 2000})
product3shop1lenovo = Companies.Product.create(%{name: "Thinkpad Carbon 4th", price: 3500})
product3shop2lenovo = Companies.Product.create(%{name: "Thinkpad W560", price: 1700})
product3shop3lenovo = Companies.Product.create(%{name: "Thinkpad X220", price: 2800})

product1shop1biedronka = Companies.Product.create(%{name: "Lizak Chupa-Chups", price: 1})
product1shop2biedronka = Companies.Product.create(%{name: "Bułka kajzerka", price: 0.25})
product1shop3biedronka = Companies.Product.create(%{name: "Czekolada Wedel", price: 1.99})
product2shop1biedronka = Companies.Product.create(%{name: "Kubuś", price: 12.34})
product2shop2biedronka = Companies.Product.create(%{name: "Sok pomarańczowy", price: 3.45})
product2shop3biedronka = Companies.Product.create(%{name: "Mars", price: 1.33})
product3shop1biedronka = Companies.Product.create(%{name: "Puzzle", price: 34.56})
product3shop2biedronka = Companies.Product.create(%{name: "Cukier 1kg", price: 5.45})
product3shop3biedronka = Companies.Product.create(%{name: "Mąka 1kg", price: 2.36})

shipment_shop1samsung = Companies.Shipment.create(%{date: ~N[2019-04-15 10:30:12]})
shipment_shop2samsung = Companies.Shipment.create(%{date: ~N[2019-04-18 19:32:19]})
shipment_shop3samsung = Companies.Shipment.create(%{date: ~N[2019-04-25 11:35:33]})

shipment_shop1lenovo = Companies.Shipment.create(%{date: ~N[2019-02-12 10:30:12]})
shipment_shop2lenovo = Companies.Shipment.create(%{date: ~N[2019-04-23 15:35:53]})
shipment_shop3lenovo = Companies.Shipment.create(%{date: ~N[2019-04-04 14:41:32]})

shipment_shop1biedronka = Companies.Shipment.create(%{date: ~N[2019-03-20 08:43:55]})
shipment_shop2biedronka = Companies.Shipment.create(%{date: ~N[2019-04-12 21:56:13]})
shipment_shop3biedronka = Companies.Shipment.create(%{date: ~N[2019-05-24 17:46:01]})

Companies.ProductHasShipment.create(%{product_id: product1shop1samsung.id, shipment_id: shipment_shop1samsung.id, quantity: 12})
Companies.ProductHasShipment.create(%{product_id: product2shop1samsung.id, shipment_id: shipment_shop1samsung.id, quantity: 8})
Companies.ProductHasShipment.create(%{product_id: product3shop1samsung.id, shipment_id: shipment_shop1samsung.id, quantity: 11})

Companies.ProductHasShipment.create(%{product_id: product1shop2samsung.id, shipment_id: shipment_shop2samsung.id, quantity: 6})
Companies.ProductHasShipment.create(%{product_id: product2shop2samsung.id, shipment_id: shipment_shop2samsung.id, quantity: 14})
Companies.ProductHasShipment.create(%{product_id: product3shop2samsung.id, shipment_id: shipment_shop2samsung.id, quantity: 22})

Companies.ProductHasShipment.create(%{product_id: product1shop3samsung.id, shipment_id: shipment_shop3samsung.id, quantity: 18})
Companies.ProductHasShipment.create(%{product_id: product2shop3samsung.id, shipment_id: shipment_shop3samsung.id, quantity: 20})
Companies.ProductHasShipment.create(%{product_id: product3shop3samsung.id, shipment_id: shipment_shop3samsung.id, quantity: 11})

Companies.ProductHasShipment.create(%{product_id: product1shop1lenovo.id, shipment_id: shipment_shop1lenovo.id, quantity: 32})
Companies.ProductHasShipment.create(%{product_id: product2shop1lenovo.id, shipment_id: shipment_shop1lenovo.id, quantity: 3})
Companies.ProductHasShipment.create(%{product_id: product3shop1lenovo.id, shipment_id: shipment_shop1lenovo.id, quantity: 5})

Companies.ProductHasShipment.create(%{product_id: product1shop2lenovo.id, shipment_id: shipment_shop2lenovo.id, quantity: 15})
Companies.ProductHasShipment.create(%{product_id: product2shop2lenovo.id, shipment_id: shipment_shop2lenovo.id, quantity: 18})
Companies.ProductHasShipment.create(%{product_id: product3shop2lenovo.id, shipment_id: shipment_shop2lenovo.id, quantity: 12})

Companies.ProductHasShipment.create(%{product_id: product1shop3lenovo.id, shipment_id: shipment_shop3lenovo.id, quantity: 11})
Companies.ProductHasShipment.create(%{product_id: product2shop3lenovo.id, shipment_id: shipment_shop3lenovo.id, quantity: 8})
Companies.ProductHasShipment.create(%{product_id: product3shop3lenovo.id, shipment_id: shipment_shop3lenovo.id, quantity: 9})

Companies.ProductHasShipment.create(%{product_id: product1shop1biedronka.id, shipment_id: shipment_shop1biedronka.id, quantity: 459})
Companies.ProductHasShipment.create(%{product_id: product2shop1biedronka.id, shipment_id: shipment_shop1biedronka.id, quantity: 214})
Companies.ProductHasShipment.create(%{product_id: product3shop1biedronka.id, shipment_id: shipment_shop1biedronka.id, quantity: 113})

Companies.ProductHasShipment.create(%{product_id: product1shop2biedronka.id, shipment_id: shipment_shop2biedronka.id, quantity: 421})
Companies.ProductHasShipment.create(%{product_id: product2shop2biedronka.id, shipment_id: shipment_shop2biedronka.id, quantity: 231})
Companies.ProductHasShipment.create(%{product_id: product3shop2biedronka.id, shipment_id: shipment_shop2biedronka.id, quantity: 421})

Companies.ProductHasShipment.create(%{product_id: product1shop3biedronka.id, shipment_id: shipment_shop3biedronka.id, quantity: 131})
Companies.ProductHasShipment.create(%{product_id: product2shop3biedronka.id, shipment_id: shipment_shop3biedronka.id, quantity: 213})
Companies.ProductHasShipment.create(%{product_id: product3shop3biedronka.id, shipment_id: shipment_shop3biedronka.id, quantity: 124})

Companies.ProductHasShop.create(%{product_id: product1shop1samsung.id, shop_id: shop1samsung.id, quantity: 12})
Companies.ProductHasShop.create(%{product_id: product2shop1samsung.id, shop_id: shop1samsung.id, quantity: 8})
Companies.ProductHasShop.create(%{product_id: product3shop1samsung.id, shop_id: shop1samsung.id, quantity: 11})

Companies.ProductHasShop.create(%{product_id: product1shop2samsung.id, shop_id: shop2samsung.id, quantity: 6})
Companies.ProductHasShop.create(%{product_id: product2shop2samsung.id, shop_id: shop2samsung.id, quantity: 14})
Companies.ProductHasShop.create(%{product_id: product3shop2samsung.id, shop_id: shop2samsung.id, quantity: 22})

Companies.ProductHasShop.create(%{product_id: product1shop3samsung.id, shop_id: shop3samsung.id, quantity: 18})
Companies.ProductHasShop.create(%{product_id: product2shop3samsung.id, shop_id: shop3samsung.id, quantity: 20})
Companies.ProductHasShop.create(%{product_id: product3shop3samsung.id, shop_id: shop3samsung.id, quantity: 11})

Companies.ProductHasShop.create(%{product_id: product1shop1lenovo.id, shop_id: shop1lenovo.id, quantity: 32})
Companies.ProductHasShop.create(%{product_id: product2shop1lenovo.id, shop_id: shop1lenovo.id, quantity: 3})
Companies.ProductHasShop.create(%{product_id: product3shop1lenovo.id, shop_id: shop1lenovo.id, quantity: 5})

Companies.ProductHasShop.create(%{product_id: product1shop2lenovo.id, shop_id: shop2lenovo.id, quantity: 15})
Companies.ProductHasShop.create(%{product_id: product2shop2lenovo.id, shop_id: shop2lenovo.id, quantity: 18})
Companies.ProductHasShop.create(%{product_id: product3shop2lenovo.id, shop_id: shop2lenovo.id, quantity: 12})

Companies.ProductHasShop.create(%{product_id: product1shop3lenovo.id, shop_id: shop3lenovo.id, quantity: 11})
Companies.ProductHasShop.create(%{product_id: product2shop3lenovo.id, shop_id: shop3lenovo.id, quantity: 8})
Companies.ProductHasShop.create(%{product_id: product3shop3lenovo.id, shop_id: shop3lenovo.id, quantity: 9})

Companies.ProductHasShop.create(%{product_id: product1shop1biedronka.id, shop_id: shop1biedronka.id, quantity: 459})
Companies.ProductHasShop.create(%{product_id: product2shop1biedronka.id, shop_id: shop1biedronka.id, quantity: 214})
Companies.ProductHasShop.create(%{product_id: product3shop1biedronka.id, shop_id: shop1biedronka.id, quantity: 113})

Companies.ProductHasShop.create(%{product_id: product1shop2biedronka.id, shop_id: shop2biedronka.id, quantity: 421})
Companies.ProductHasShop.create(%{product_id: product2shop2biedronka.id, shop_id: shop2biedronka.id, quantity: 231})
Companies.ProductHasShop.create(%{product_id: product3shop2biedronka.id, shop_id: shop2biedronka.id, quantity: 421})

Companies.ProductHasShop.create(%{product_id: product1shop3biedronka.id, shop_id: shop3biedronka.id, quantity: 131})
Companies.ProductHasShop.create(%{product_id: product2shop3biedronka.id, shop_id: shop3biedronka.id, quantity: 213})
Companies.ProductHasShop.create(%{product_id: product3shop3biedronka.id, shop_id: shop3biedronka.id, quantity: 124})
