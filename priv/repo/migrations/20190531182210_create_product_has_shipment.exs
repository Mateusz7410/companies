defmodule Companies.Repo.Migrations.CreateProductHasShipment do
  use Ecto.Migration

  def change do
    create table(:products_have_shipments) do
      add(:product_id, references("products", on_delete: :delete_all))
      add(:shipment_id, references("shipments", on_delete: :delete_all))
      add(:quantity, :integer, null: false)
      timestamps()
    end
  end
end
