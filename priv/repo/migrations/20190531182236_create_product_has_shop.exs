defmodule Companies.Repo.Migrations.CreateProductHasShop do
  use Ecto.Migration

  def change do
    create table(:products_have_shops) do
      add(:product_id, references("products", on_delete: :delete_all))
      add(:shop_id, references("shops", on_delete: :delete_all))
      add(:quantity, :integer, null: false)
      timestamps()
    end
  end
end
