defmodule Companies.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add(:name, :text, null: false)
      add(:price, :decimal, null: false)
      timestamps()
    end
  end
end
