defmodule Companies.Repo.Migrations.CreateShipments do
  use Ecto.Migration

  def change do
    create table(:shipments) do
      add(:shop_id, references("shops", on_delete: :delete_all))
      add(:date, :utc_datetime, null: false)
      timestamps()
    end
  end
end
