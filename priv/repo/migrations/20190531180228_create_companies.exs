defmodule Companies.Repo.Migrations.CreateCompanies do
  use Ecto.Migration

  def change do
    create table(:companies) do
      add(:name, :text, null: false)
      add(:ceo, :text, null: false)
      timestamps()
    end
  end
end
