defmodule Companies.Repo.Migrations.CreateShops do
  use Ecto.Migration

  def change do
    create table(:shops) do
      add(:company_id, references("companies", on_delete: :delete_all))
      add(:address, :text, null: false)
      add(:director, :text, null: false)
      timestamps()
    end
  end
end
