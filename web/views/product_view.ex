defmodule Companies.ProductView do
  use Companies.Web, :view

  def render("products.json", %{products: products}) do
    %{
      products: Enum.map(products.entries, &product_json(&1)),
      after: products.metadata.after,
      before: products.metadata.before,
      total_page_count: Float.ceil(products.metadata.total_count / products.metadata.limit)
    }
  end

  def product_json(product) do
    %{
      id: product.id,
      name: product.name,
      price: Decimal.to_string(product.price)
    }
  end

  def render("product.json", %{product: product}) do
    product_json(product)
  end
end
