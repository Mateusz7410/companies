defmodule Companies.ShopView do
  use Companies.Web, :view

  def render("shops_short.json", %{shops: shops}) do
    %{
      shops: Enum.map(shops, &shop_short_json(&1))
    }
  end

  def shop_short_json(shop) do
    %{
      id: shop.id,
      name: shop.address,
      ceo: shop.director
    }
  end

  def render("shops_long.json", %{shops: shops}) do
    %{
      shops: Enum.map(shops, &shop_long_json(&1))
    }
  end

  def shop_long_json(shop) do
    %{
      id: shop.id,
      name: shop.address,
      ceo: shop.director,
      company_name: shop.company.name,
      products: Enum.map(shop.products_have_shops, &product_json(&1))
    }
  end

  def product_json(product) do
    %{
      id: product.product.id,
      name: product.product.name,
      price: Decimal.to_string(product.product.price)
    }
  end

  def render("shop_short.json", %{shop: shop}) do
    shop_short_json(shop)
  end

  def render("shop_long.json", %{shop: shop}) do
    shop_long_json(shop)
  end
end
