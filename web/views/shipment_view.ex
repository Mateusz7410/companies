defmodule Companies.ShipmentView do
  use Companies.Web, :view

  def render("shipments.json", %{shipments: shipments, shop_id: shop_id}) do
    shop = Companies.Repo.get_by(Companies.Shop, id: shop_id)
    shipments =
    shipments
    |> Enum.map(&shipment_json(&1))
    %{
      shop_id: shop.id,
      shop_director: shop.director,
      shipments: shipments
    }
  end

  def render("shipments.json", %{shipments: shipments, company_id: company_id}) do
    company = Companies.Repo.get_by(Companies.Company, id: company_id)
    shipments =
    shipments
    |> Enum.map(&shipment_json(&1))
    %{
      company_id: company.id,
      company_name: company.name,
      company_ceo: company.ceo,
      shipments: shipments
    }
  end

  def shipment_json(shipment) do
    %{
      id: shipment.id,
      date: shipment.date
    }
  end

  def product_json(product) do
    %{
      id: product.id,
      name: product.name,
      price: Decimal.to_string(product.price)
    }
  end

  def render("shipment.json", %{shipment: shipment, shop_id: shop_id}) do
    shop = Companies.Repo.get_by(Companies.Shop, id: shop_id)
    %{
      shop_id: shop.id,
      shop_director: shop.director,
      shipment_id: shipment.id,
      date: shipment.date,
      products: Enum.map(shipment.products_have_shipments, &product_json(&1.product))
    }
  end
end
