defmodule Companies.CompanyView do
  use Companies.Web, :view

  def render("companies_short.json", %{companies: companies}) do
    %{
      companies: Enum.map(companies, &company_short_json(&1))
    }
  end

  def company_short_json(company) do
    %{
      id: company.id,
      name: company.name,
      ceo: company.ceo
    }
  end

  def render("companies_long.json", %{companies: companies}) do
    %{
      companies: Enum.map(companies, &company_long_json(&1))
    }
  end

  def company_long_json(company) do
    %{
      id: company.id,
      name: company.name,
      ceo: company.ceo,
      shops: Enum.map(company.shops, &shop_json(&1))
    }
  end

  def shop_json(shop) do
    %{
      id: shop.id,
      address: shop.address,
      director: shop.director
    }
  end

  def render("company_short.json", %{company: company}) do
    company_short_json(company)
  end

  def render("company_long.json", %{company: company}) do
    company_long_json(company)
  end

  def render("company_products.json", %{products: products}) do
    %{
      id: products.company.id,
      name: products.company.name,
      ceo: products.company.ceo,
      products: Enum.map(products.products, &product_json(&1))
    }
  end

  def product_json(product) do
    %{
      id: product.product.id,
      name: product.product.name,
      price: Decimal.to_string(product.product.price)
    }
  end
end
