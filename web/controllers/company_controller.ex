defmodule Companies.CompanyController do
  use Companies.Web, :controller

  def get(conn, %{"id" => id, "type" => "long"}) do
    case Companies.CompanyFunctions.get_long(id) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      company ->
        data = Companies.CompanyView.render("company_long.json", %{company: company})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def get(conn, %{"id" => id}) do
    case Companies.CompanyFunctions.get_short(id) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      company ->
        data = Companies.CompanyView.render("company_short.json", %{company: company})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def get_products(conn, %{"id" => id}) do
    case Companies.CompanyFunctions.get_products(id) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      products ->
        data = Companies.CompanyView.render("company_products.json", %{products: products})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def get(conn, %{"type" => "long"}) do
    companies = Companies.CompanyFunctions.get_long()
    data = Companies.CompanyView.render("companies_long.json", %{companies: companies})

    conn
    |> put_status(200)
    |> json(data)
  end

  def get(conn, %{}) do
    companies = Companies.CompanyFunctions.get_short()
    data = Companies.CompanyView.render("companies_short.json", %{companies: companies})

    conn
    |> put_status(200)
    |> json(data)
  end

  def post(conn, params) do
    case Companies.CompanyFunctions.post(params) do
      :token ->
        conn
        |> put_status(400)
        |> json(%{error: "You already sent POST to server or didnt get token."})
      _company ->
        conn
        |> put_status(201)
        |> json([])
    end
  end

  def put(conn, params) do
    case Companies.CompanyFunctions.put(params) do
      {:preconditon_failed, token} ->
        conn
        |> put_status(412)
        |> json(%{error: "Precondition failed", token: token})
      {:precondition_required, token} ->
        conn
        |> put_status(428)
        |> json(%{error: "Precondition required", token: token})
      :error ->
        conn
        |> put_status(410)
        |> json(%{error: "No company with given id"})
      token ->
        conn
        |> put_status(201)
        |> json(%{token: token})
    end
  end

  def delete(conn, %{"id" => id}) do
    case Companies.CompanyFunctions.delete(id) do
      :error ->
        conn
        |> put_status(410)
        |> json(%{error: "No company with given id"})
      _company ->
        conn
        |> put_status(200)
        |> json([])
    end
  end
end
