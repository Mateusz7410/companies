defmodule Companies.ProductController do
  use Companies.Web, :controller

  def get(conn, %{"company_id" => company_id, "shop_id" => shop_id, "product_id" => product_id}) do
    case Companies.ProductFunctions.get(company_id, shop_id, product_id) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      product ->
        data = Companies.ProductView.render("product.json", %{product: product})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def get(conn, %{"company_id" => company_id, "shop_id" => shop_id, "pagination" => pagination, "token" => token}) do
    case Companies.ProductFunctions.get(company_id, shop_id, pagination, token) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      products ->
        data = Companies.ProductView.render("products.json", %{products: products})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def get(conn, %{"company_id" => company_id, "shop_id" => shop_id}) do
    case Companies.ProductFunctions.get(company_id, shop_id, "first", nil) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      products ->
        data = Companies.ProductView.render("products.json", %{products: products})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def post(conn, params) do
    Companies.ProductFunctions.post(params)

    conn
    |> put_status(201)
    |> json([])
  end

  def put(conn, params) do
    case Companies.ProductFunctions.put(params) do
      :error ->
        conn
        |> put_status(409)
        |> json(%{error: "No shop/company/product with given id"})
      _product ->
        conn
        |> put_status(204)
        |> json([])
    end
  end

  def delete(conn, %{"company_id" => company_id, "shop_id" => shop_id, "product_id" => product_id}) do
    case Companies.ProductFunctions.delete(product_id) do
      :error ->
        conn
        |> put_status(409)
        |> json(%{error: "No product with given id"})
      _product ->
        conn
        |> put_status(200)
        |> json([])
    end
  end

  def delete(conn, %{"company_id" => company_id, "shop_id" => shop_id}) do
    case Companies.ProductFunctions.delete(company_id, shop_id) do
      :error ->
        conn
        |> put_status(409)
        |> json(%{error: "No shop/company with given id"})
      _product ->
        conn
        |> put_status(200)
        |> json([])
    end
  end

  def patch(conn, params) do
    case Companies.ProductFunctions.put(params) do
      :error ->
        conn
        |> put_status(409)
        |> json(%{error: "No shop/company/product with given id"})
      _product ->
        conn
        |> put_status(204)
        |> json([])
    end
  end
end
