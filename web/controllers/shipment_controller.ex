defmodule Companies.ShipmentController do
  use Companies.Web, :controller

  def get(conn, %{"shop_id" => shop_id, "shipment_id" => shipment_id}) do
    case Companies.ShipmentFunctions.get(shop_id, shipment_id) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      shipment ->
        data = Companies.ShipmentView.render("shipment.json", %{shipment: shipment, shop_id: shop_id})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def get(conn, %{"company_id" => company_id}) do
    case Companies.ShipmentFunctions.get_all_from_company(company_id) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      shipments ->
        data = Companies.ShipmentView.render("shipments.json", %{shipments: shipments, company_id: company_id})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def get(conn, %{"shop_id" => shop_id}) do
    case Companies.ShipmentFunctions.get_all_from_shop(shop_id) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      shipments ->
        data = Companies.ShipmentView.render("shipments.json", %{shipments: shipments, shop_id: shop_id})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def post(conn, params) do
    case Companies.ShipmentFunctions.post(params) do
      :ok ->
        conn
        |> put_status(201)
        |> json([])
      :error ->
        conn
        |> put_status(400)
        |> json(%{error: "One of products have wrong id. No changes were written."})
    end
  end

  def delete(conn, %{"shop_id" => shop_id, "shipment_id" => shipment_id}) do
    case Companies.ShipmentFunctions.delete(shipment_id) do
      :error ->
        conn
        |> put_status(409)
        |> json(%{error: "No shipment with given id or some of the products was sold."})
      _shipment ->
        conn
        |> put_status(200)
        |> json([])
    end
  end
end
