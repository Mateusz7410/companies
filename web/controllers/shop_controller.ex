defmodule Companies.ShopController do
  use Companies.Web, :controller

  def get(conn, %{"company_id" => company_id, "shop_id" => shop_id, "type" => "long"}) do
    case Companies.ShopFunctions.get_long(company_id, shop_id) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      shop ->
        data = Companies.ShopView.render("shop_long.json", %{shop: shop})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def get(conn, %{"company_id" => company_id, "shop_id" => shop_id}) do
    case Companies.ShopFunctions.get_short(company_id, shop_id) do
      :error ->
        conn
        |> put_status(200)
        |> json([])
      shop ->
        data = Companies.ShopView.render("shop_short.json", %{shop: shop})

        conn
        |> put_status(200)
        |> json(data)
    end
  end

  def get(conn, %{"company_id" => company_id, "type" => "long"}) do
    shops = Companies.ShopFunctions.get_long(company_id)
    data = Companies.ShopView.render("shops_long.json", %{shops: shops})

    conn
    |> put_status(200)
    |> json(data)
  end

  def get(conn, %{"company_id" => company_id}) do
    shops = Companies.ShopFunctions.get_short(company_id)
    data = Companies.ShopView.render("shops_short.json", %{shops: shops})

    conn
    |> put_status(200)
    |> json(data)
  end

  def post(conn, params) do
    case Companies.ShopFunctions.post(params) do
      :error ->
        conn
        |> put_status(400)
        |> json(%{error: "No shop/company with given id"})
      _shop ->
        conn
        |> put_status(201)
        |> json([])
    end
  end

  def put(conn, params) do
    case Companies.ShopFunctions.put(params) do
      :error ->
        conn
        |> put_status(409)
        |> json(%{error: "No shop/company with given id"})
      _shop ->
        conn
        |> put_status(204)
        |> json([])
    end
  end

  def delete(conn, %{"company_id" => company_id, "shop_id" => shop_id}) do
    case Companies.ShopFunctions.delete(company_id, shop_id) do
      :error ->
        conn
        |> put_status(409)
        |> json(%{error: "No shop/company with given id"})
      _shop ->
        conn
        |> put_status(200)
        |> json([])
    end
  end
end
