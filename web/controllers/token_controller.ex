defmodule Companies.TokenController do
  use Companies.Web, :controller

  def get(conn, _params) do
    token = random_string(64)
    Companies.TokenAgent.put(token, token)
    conn
    |> put_status(200)
    |> json(%{token: token})
  end

  def random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64 |> binary_part(0, length)
  end
end
