defmodule Companies.Router do
  use Companies.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :accepts, ["json"]
  end

  scope "/", Companies do
    pipe_through :browser

    get "/companies", CompanyController, :get
    get "/companies/:id", CompanyController, :get
    post "/companies", CompanyController, :post
    put "/companies/:id", CompanyController, :put
    delete "/companies/:id", CompanyController, :delete
    get "/companies/:id/products", CompanyController, :get_products

    get "/companies/:company_id/shops", ShopController, :get
    get "/companies/:company_id/shops/:shop_id", ShopController, :get
    post "/companies/:company_id/shops", ShopController, :post
    put "/companies/:company_id/shops/:shop_id", ShopController, :put
    delete "/companies/:company_id/shops/:shop_id", ShopController, :delete

    get "/companies/:company_id/shops/:shop_id/products", ProductController, :get
    get "/companies/:company_id/shops/:shop_id/products/:product_id", ProductController, :get
    post "/companies/:company_id/shops/:shop_id/products", ProductController, :post
    put "/companies/:company_id/shops/:shop_id/products/:product_id", ProductController, :put
    delete "/companies/:company_id/shops/:shop_id/products", ProductController, :delete
    delete "/companies/:company_id/shops/:shop_id/products/:product_id", ProductController, :delete
    patch "/companies/:company_id/shops/:shop_id/products/:product_id", ProductController, :patch

    get "/companies/:company_id/shipments", ShipmentController, :get
    get "/shops/:shop_id/shipments", ShipmentController, :get
    get "/shops/:shop_id/shipments/:shipment_id", ShipmentController, :get
    post "/shops/:shop_id/shipments", ShipmentController, :post
    delete "/shops/:shop_id/shipments/:shipment_id", ShipmentController, :delete

    get "/post", TokenController, :get
  end
end
